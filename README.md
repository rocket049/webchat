##配置
数据库类型：`postgres`

配置文件：`conf.toml`

配置示例：

```
WWWAddr =":8080"             #服务器地址和端口，可以再冒号前指定ＩＰ
ViewDir = "./views"　　　     #template目录
StaticDir = "./static"　　    #js、css目录
SessionDir = "./sessions"　  #session保存目录
Prefix = ""　　　　　　　       #如果你使用代理服务器，可以路径前面加上前缀
DbAddr = "127.0.0.1:5432"　　　#数据库服务器
DbName = "chatdb"             #数据库名字
DbUser = "dbusername"         #数据库用户名
DbPwd = "dbpassword"          #数据库密码
Rooms = ["聊天室","go语言","Linux系统"]   #指定显示的聊天室名字
```

##apache2代理配置

一个例子：

```
<VirtualHost chat.loc>
    ProxyPass /          http://chat.loc:8666/    enablereuse=on
    ProxyPassReverse /   http://chat.loc:8666/    enablereuse=on
</VirtualHost>
```

**注意**，内部地址`http://chat.loc:8666/`的`hostname`必须和`VirtualHost`名字相同，
否则会导致`websocket`连接时`session id`变化，进而导致聊天室无法获得正确的登录状态。
因此必须编辑`apache2`所在主机的`hosts`文件正确设置内部的域名解析规则。
