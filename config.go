package main

import (
	"log"

	"github.com/BurntSushi/toml"
)

//config 结构体对应 conf.toml
type config struct {
	WWWAddr    string
	ViewDir    string
	StaticDir  string
	SessionDir string
	Prefix     string
	DbAddr     string
	DbName     string
	DbUser     string
	DbPwd      string
	Rooms      []string
}

func readToml(filename string) *config {
	var conf1 = new(config)
	_, err := toml.DecodeFile(filename, conf1)
	if err == nil {
		return conf1
	} else {
		log.Printf("readToml: %v\n", err)
		return nil
	}
}
