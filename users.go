package main

import (
	"crypto/md5"
	"encoding/hex"
	"strings"

	"github.com/kataras/iris"
)

func (site1 *Site) setUsersHandlers() {
	site1.App.Get("/user/reg", site1.UserReg)
	site1.App.Post("/user/save", site1.UserSave)
	site1.App.Post("/user/login", site1.UserLogin)
	site1.App.Get("/user/", site1.UserLoginPage)
	site1.App.Get("/user/logout", site1.UserLogout)
}

//UserReg 注册用户 GET /user/reg
func (site1 *Site) UserReg(ctx iris.Context) {
	v := ctx.GetCookie("logined")
	var data iris.Map
	if v == "yes" {
		data = iris.Map{"Prefix": site1.Conf.Prefix,
			"title": "你已经登录了！",
			"mode":  1}
		ctx.ViewData("", data)
		ctx.View("login.html")
	} else {
		data = iris.Map{"Prefix": site1.Conf.Prefix}
		ctx.ViewData("", data)
		ctx.View("reguser.html")
	}
}

type FormSave struct {
	Name string `form:"name"`
	Pwd  string `form:"pwd"`
}

//UserSave 保存新用户 POST /user/save
func (site1 *Site) UserSave(ctx iris.Context) {
	user := new(FormSave)
	ctx.ReadForm(user)
	if len(user.Name) == 0 || len(user.Pwd) == 0 {
		ctx.Writef("ERROR: 含有空数据 %#v", user)
		return
	}
	if strings.Contains(user.Name, " ") {
		ctx.WriteString("ERROR: 名字不得含有空格")
		return
	}
	sql1 := "insert into users (name, md5pwd) values ( ?, ? )"
	db := site1.Db.Conn
	_, err := db.Exec(sql1, user.Name, user.Pwd)
	if err != nil {
		ctx.Writef("ERROR:%s", err.Error())
	} else {
		ctx.Writef("SUCCESS:%s", user.Name)
	}
}

type FormLogin FormSave

//UserLogin 登录 POST /user/login
func (site1 *Site) UserLogin(ctx iris.Context) {
	user := new(FormLogin)
	ctx.ReadForm(user)
	s1 := site1.Sesns.Start(ctx)
	vs1 := site1.getValidStr(user.Name, s1.GetString("start"), ctx.GetCookie("addr"))
	if vs1 == user.Pwd {
		//success
		s1.Set("logined", "yes")
		s1.Set("chatname", user.Name)
		ctx.SetCookieKV("chatname", user.Name)
		ctx.SetCookieKV("logined", "yes")
		data := iris.Map{"Prefix": site1.Conf.Prefix,
			"title": "你已经登录了！",
			"mode":  1}
		ctx.ViewData("", data)
		ctx.View("login.html")
	} else {
		//fail
		s1.Set("logined", "no")
		s1.Set("chatname", "")
		ctx.SetCookieKV("chatname", "")
		ctx.SetCookieKV("logined", "no")
		s1.Set("start", ctx.GetCookie("start"))
		ctx.Redirect("/user/")
	}
}
func hexMd5(s string) string {
	enc1 := md5.Sum([]byte(s))
	return hex.EncodeToString(enc1[:])
}
func (site1 *Site) getValidStr(name, start, addr string) string {
	sql1 := "select md5pwd from users where name=?"
	var md5pwd string
	db := site1.Db.Conn
	_, err := db.QueryOne(&md5pwd, sql1, name)
	if err != nil {
		site1.App.Logger().Printf("getValidStr: %s", err.Error())
		return ""
	}
	str1 := start + addr + md5pwd
	return hexMd5(str1)
}

//UserLoginPage Get /user/
func (site1 *Site) UserLoginPage(ctx iris.Context) {
	v := ctx.GetCookie("logined")
	var data iris.Map
	if v == "yes" {
		data = iris.Map{"Prefix": site1.Conf.Prefix,
			"title": "你已经登录了！",
			"mode":  1}
	} else {
		data = iris.Map{"Prefix": site1.Conf.Prefix,
			"title": "请登录！",
			"mode":  0}
	}
	ctx.ViewData("", data)
	ctx.View("login.html")
}

//UserLogout GET /user/logout
func (site1 *Site) UserLogout(ctx iris.Context) {
	s1 := site1.Sesns.Start(ctx)
	s1.Set("logined", "no")
	s1.Set("chatname", "")
	ctx.SetCookieKV("chatname", "")
	ctx.SetCookieKV("logined", "no")
	ctx.StatusCode(200)
	data := iris.Map{"Prefix": site1.Conf.Prefix,
		"title": "请登录！",
		"mode":  0}
	ctx.ViewData("", data)
	ctx.View("login.html")
}
